require('dotenv').config()

const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const http = require('http')
const db = require('./db')
const middlewares = require('./middlewares')
const ws = require('./ws')

const multer = require('multer')
const upload = multer({ storage: multer.memoryStorage() })
const fetch = require('node-fetch')
const FormData = require('form-data')

const port = process.env.PORT || 8080
const secret = process.env.SECRET || 'change-me-2021'
const imgurClientId = process.env.CLIENT_ID

const app = express()
const server = http.createServer(app)

app
  .use(cors())
  .use(bodyParser.json())

app.get('/', (req, res) => {
  res.send('Api de ejemplo para alumnos de HAB')
})

app.get('/login', (req, res) => {
  res.status(400).send({ error: 'Login debe usarse con POST!' })
})

app.post('/login', middlewares.withFields('username', 'password'), async (req, res) => {
  try {
    const user = await db.getUserByUsername(req.body.username)
    if (!user) {
      res.status(401).send({ error: 'Usuario desconocido' })
    } else {
      const isOk = await bcrypt.compare(req.body.password, user.password)
      if (!isOk) {
        res.status(401).send({ error: 'Contraseña incorrecta' })
      } else {
        delete user.password
        res.send({
          ...user,
          token: jwt.sign(user.id, secret)
        })
      }
    }
  } catch (e) {
    res.status(500).send({ error: 'Error del servidor' })
  }
})

app.get('/register', (req, res) => {
  res.status(400).send({ error: 'Register debe usarse con POST!' })
})

app.post('/register', middlewares.withFields('username', 'password'), async (req, res) => {
  try {
    const user = await db.addUser({
      username: req.body.username,
      password: await bcrypt.hash(req.body.password, 12),
      avatar: 'https://i.imgur.com/CevZ3gf.jpg'
    })
    delete user.password
    res.send({
      ...user,
      token: jwt.sign(user.id, secret)
    })
  } catch (e) {
    res.status(400).send({ error: 'El usuario existe' })
  }
})

app.get('/chats', middlewares.withAuth(), async (req, res) => {
  const uid = req.auth.uid
  try {
    const users = await db.getContacts(uid)
    const msgs = await db.getLastMessages(uid)
    const ret = users.map(u => {
      const m = msgs.find(m => m.src === u.id || m.dst === u.id)
      return { ...u, lastMessage: m }
    })
    ret.sort((a, b) => {
      const ad = a.lastMessage?.date || 0
      const bd = b.lastMessage?.date || 0
      if (ad !== bd) return ad < bd ? 1 : -1
      return a.username < b.username ? 1 : -1
    })
    res.send(ret)
  } catch (e) {
    res.status(500).send({ error: 'Error del servidor' })
  }
})

app.get('/chats/:id', middlewares.withAuth(), async (req, res) => {
  const uid = req.auth.uid
  const target = req.params.id
  if (target === uid) {
    res.status(404).send({ error: 'Chat no encontrado'})
    return
  }
  try {
    const targetUser = await db.getContact(target)
    if (!targetUser) {
      res.status(404).send({ error: 'Chat no encontrado'})
      return
    }
    const before = req.query.before ? new Date(req.query.before) : null
    const messages = await db.getMessages(uid, target, before)
    res.send({
      user: targetUser,
      messages
    })
  }catch (e) {
    res.status(500).send({ error: 'Error del servidor' })
  }
})

app.post('/chats/:id', middlewares.withAuth(), middlewares.withFields('message'), async (req, res) => {
  const uid = req.auth.uid
  const target = req.params.id
  if (target === uid) {
    res.status(404).send({ error: 'Chat no encontrado'})
    return
  }
  try {
    const targetUser = await db.getContact(target)
    if (!targetUser) {
      res.status(404).send({ error: 'Chat no encontrado'})
      return
    }
    const message = await db.addMessage(uid, target, req.body.message)
    res.send({
      user: targetUser,
      message
    })
    ws.send(message)
  }catch (e) {
    res.status(500).send({ error: 'Error del servidor' })
  }
})

app.post('/avatar', middlewares.withAuth(), upload.single('avatar'), async (req, res) => {
  const uid = req.auth.uid
  try {
    const body = new FormData()
    body.append('image', req.file.buffer)
    const r = await fetch('https://api.imgur.com/3/image', {
      method: 'POST',
      headers: { 'Authorization': 'Client-ID ' + imgurClientId },
      body
    })
    const info = await r.json()
    if (r.ok && info.data.link) {
      const url = info.data.link
      await db.updateAvatar(uid, url)
      res.send({ avatar: url })
    } else {
      console.log(r.status, info)
      throw new Error('Error uploading file')
    }
  } catch (e) {
    console.warn(e)
    res.status(500).send({ error: 'Error del servidor' })
  }
})

app.get('*', function(req, res){
  res.status(404).send({ error: 'Ruta desconocida' })
})

app.use(function(err, req, res, next) {
  console.error(err.stack);
  res.status(500).send({ error: 'Error del servidor' })
})

ws.init(server)
server.listen(port)
console.log(`API listening at http://localhost:${port}`)
