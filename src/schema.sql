create table "user"(
  id serial primary key,
  username text not null unique,
  password text not null,
  avatar text not null
);

create table message(
  id serial primary key,
  src integer not null references "user"(id),
  dst integer not null references "user"(id),
  message text not null,
  date timestamp not null default CURRENT_TIMESTAMP,
  status int not null default 0
);
