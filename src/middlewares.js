const jwt = require('jsonwebtoken')
const secret = process.env.SECRET || 'change-me-2021'

const withAuth = () => {
  return (req, res, next) => {
    if (!req.headers['authorization']) {
        res.status(401).send({ error: 'Se requiere autenticación'})
    } else {
      try {
        uid = jwt.verify(req.headers.authorization.substr(7), secret)
        req.auth = { uid }
        next()
      } catch (e) {
        res.status(401).send({ error: 'Token no válido'})
      }
    }
  }
}

const withFields = (...fields) => {
  return (req, res, next) => {
    if (fields.some(f => !req.body[f])) {
      res.status(400).send({ error: 'Falta algún campo obligatorio: ' + fields.join(', ')})
    } else {
      next()
    }
  }
}

exports = module.exports = {
  withAuth,
  withFields
}
