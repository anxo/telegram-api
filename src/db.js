require('dotenv').config()

const { Pool } = require('pg')

const connectionString = process.env.DATABASE_URL

if (!connectionString) throw new Error('No DB has been configured!')
const pool = new Pool({ connectionString })

const getUserByUsername = async username => {
  const r = await pool.query('select * from "user" where username = $1', [username])
  return r.rows[0]
}

const addUser = async user => {
  const r = await pool.query(
    'insert into "user"(username, password, avatar) values ($1, $2, $3) returning *',
    [user.username, user.password, user.avatar]
  )
  return r.rows[0]
}

const updateAvatar = async (uid, url) => {
  const r = await pool.query(
    'update "user" set avatar = $1 where id = $2 returning *',
    [url, uid]
  )
  return r.rows[0]
}

const getContacts = async uid => {
  const r = await pool.query('select id, username, avatar from "user" where id != $1', [uid])
  return r.rows
}

const getLastMessages = async uid => {
  const r = await pool.query(`
    WITH latest AS (
      SELECT
        m.*,
        ROW_NUMBER() OVER(
          PARTITION BY case when m.src > m.dst then concat(m.dst, m.src) else concat(m.src, m.dst) end ORDER BY date DESC
        ) AS rank
      FROM message m
      WHERE src = $1 OR dst = $1
    )
    SELECT * FROM latest WHERE rank = 1
  `, [uid])
  return r.rows
}

const getContact = async uid => {
  const r = await pool.query('select id, username, avatar from "user" where id = $1', [uid])
  return r.rows[0]
}

const getMessages = async (uid, targetuid, before) => {
  const r = await pool.query('select * from message where date < coalesce($3, CURRENT_TIMESTAMP) and ((src=$1 and dst=$2) or (src=$2 and dst=$1)) order by date desc limit 20', [uid, targetuid, before])
  return r.rows
}

const addMessage = async (uid, targetuid, text) => {
  const r = await pool.query('insert into message (src, dst, message) values ($1, $2, $3) returning *', [uid, targetuid, text])
  return r.rows[0]
}

exports = module.exports = {
  getUserByUsername,
  addUser,
  updateAvatar,
  getContacts,
  getLastMessages,
  getContact,
  getMessages,
  addMessage
}
