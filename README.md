# Telegram API

Una API que simula un chat estilo Telegram o Whatsapp, para usar como ejemplo en clase.

## API

Estas son las rutas existentes:

- Gestión de usuarios:
  - `/login [POST]`: Inicia sesión y devuelve los datos del usuario y su token.
  - `/register [POST]`: Registra un nuevo usuario.
- Mensajería:
  - `/chats [GET]`: Lista todos los contactos, y el último mensaje con cada uno.
  - `/chats/:id [GET]`: Devuelve los mensajes de una determinada conversación.
  - `/chats/:id [POST]`: Envía un mensaje a una determinada conversación.

Todas las rutas incluyen errores descriptivos explicando su uso.
